# Endpoints

Targets:
- [X] Get all job
- [X] Filter jobs based on skill, job type (intern, part-time, full-time),
- [X] Post a job
- [X] Verify a job
- [X] Get job details
- [x] Apply to a job
- [X] List of applied jobs
- [X] List of created jobs
- [x] Delete a job (Physically delete it from DB)
- [x] Close a job (Expire the job early)

### Job
METHOD | ENDPOINT | DESCRIPTION
--- | --- | --- 
GET | `/job` | Get all available jobs.
-|| - `?status` gilter by job status
-|| - `?skills` filter based on skills requirement
-|| - `?types` filter based on type of job
-|| - `?applied` filter based on applied jobs
-|| - `?created` filter based on created jobs
POST | `/job` | Post unverified job.
GET | `/job/{slug}` | Get job details by slug.
PUT | `/job/{slug}` | Update job by slug.
DELETE | `/job/{slug}` | Physically delete a job by slug (Erase from DB).
POST | `/job/{slug}/close` | Expire the job early
POST | `/job/{slug}/apply` | Create user's job application to a job by slug.
POST | `/job/{slug}/verify` | Verify a job by slug

### Profile
METHOD | ENDPOINT | DESCRIPTION
--- | --- | --- 
PUT | `/profile/{email}` | Update profile by PK (email)
GET | `/profile/{email}` | Get LoggedIn User profile

### Auth
METHOD | ENDPOINT | DESCRIPTION
--- | --- | --- 
POST | `/login` | Post user information to log in
POST | `/register` | Post newly registered account
