# Comunicating with Mail Service
Our mail service is seperated from the main application, the source code can be found in [here](https://gitlab.com/adpro-c3/jumpstart-email).  

To communicate with the service, both mail service and this service uses [MailRequest](src/main/java/id/c3/adpro/jumpstart/auth/model/MailRequest.java) object to send email information.
Our mail service provide several type of email we can choose from different endpoints.

### Endpoints
METHOD | ENDPOINT | DESCRIPTION
--- | --- | ---  
GET | `/` | See the JSON structure that will be accepted by the service.
GET | `/ping` | Check for service's response.
POST | `/confirmationMail` | Send a confirmation mail for account activation upon registration.
POST | `/notifyMail` | Send a notification mail about a new job that match user skill.
POST | `/recruitmentMail` | Send a notification about an action as been done by an admin to a recruitment made by the user.
POST | `/applicationMail` | Send a notification about an job application status update to a user

Each endpoint can be called using WebFlux's WebClient asynchronously. 
This is a job for the [NotifyService](src/main/java/id/c3/adpro/jumpstart/auth/service/NotifyService.java),
each mail type can be called anywhere and works asynchronously. Each mail type also have their own specification:

### Specifications
- **Confirmation Mail**
    - Require login: false
    - Array content: ```[ email_address, name, confirmationToken ]```
- **Notification Mail**
    - Require login: true
    - Array content: ```[ email_address, name, job_name, skill ]```
- **Recruitment Mail**
    - Require login: true
    - Array content: ```[ email_address, name, job_name, status ]```
- **Application Mail**
    - Require login: true
    - Array content: ```[ email_address, name, job_name, status ]```
