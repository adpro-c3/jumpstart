package id.c3.adpro.jumpstart.auth.handler;

import id.c3.adpro.jumpstart.auth.model.JwtRequest;

public interface AuthHandler {
    void handle(JwtRequest request) throws Exception;
}
