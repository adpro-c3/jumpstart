package id.c3.adpro.jumpstart.auth.model;

import lombok.Data;

@Data
public class MailRequest {

    String email;
    String name;
    String[] args;

    public MailRequest(String email, String name, String[] args) {
        this.email = email;
        this.name = name;
        this.args = args;
    }
}
