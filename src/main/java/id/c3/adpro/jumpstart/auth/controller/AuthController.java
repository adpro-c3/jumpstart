package id.c3.adpro.jumpstart.auth.controller;

// mostly from https://medium.com/swlh/spring-boot-security-jwt-hello-world-example-b479e457664c

import id.c3.adpro.jumpstart.auth.config.JwtUtils;
import id.c3.adpro.jumpstart.auth.model.ConfirmationToken;
import id.c3.adpro.jumpstart.auth.model.JwtRequest;
import id.c3.adpro.jumpstart.auth.model.MailType;
import id.c3.adpro.jumpstart.auth.repository.ConfirmationTokenRepository;
import id.c3.adpro.jumpstart.auth.service.AuthService;
import id.c3.adpro.jumpstart.auth.service.NotifyService;
import id.c3.adpro.jumpstart.auth.service.UserDetailService;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import java.util.Map;

import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class AuthController {

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private AuthService authService;

    @Autowired
    private PenggunaRepository penggunaRepository;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    private NotifyService notifyService;

    @Value("${notif.domain}")
    private String domain;

    private Logger LOG = LoggerFactory.getLogger(AuthController.class);
    @Timed("login")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(
        @RequestBody JwtRequest authenticationRequest
    ) {
        ResponseEntity<?> response = null;
        try {
            Map<String, String> userData = authService.authenticate(authenticationRequest);
            response = ResponseEntity.ok(userData);
        } catch (Exception e){
            if (e.getMessage().equals("Password didn't match"))
                response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
            else {
                response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
            }
        } return response;
    }
    @Timed("regis")
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> createUserAccount(@RequestBody JwtRequest penggunaData) {

        Pengguna existingPengguna = penggunaRepository.findByEmail(penggunaData.getUsername());

        if (existingPengguna != null) {
            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("Your email address is already registered");
        } else {
            Pengguna pengguna = new Pengguna(penggunaData.getUsername(),
                penggunaData.getPassword());
            pengguna.setNama(penggunaData.getNama());
            ConfirmationToken confirmationToken = new ConfirmationToken(pengguna);

            try {
                penggunaRepository.save(pengguna);
                confirmationTokenRepository.save(confirmationToken);
                notifyService.notify(MailType.CONFIRMATION, new String[]{
                    pengguna.getUsername(),
                    pengguna.getNama(),
                    confirmationToken.getConfirmationToken()
                });
            } catch (Exception e) {
                LOG.error("Error while recording registration data", e);
                return ResponseEntity.badRequest().body("Failed to record registration data");
            }

             return ResponseEntity.ok("Registrasi Berhasil, silakan cek email Anda untuk konfirmasi");
        }

    }
    @Timed("confirm_acc")
    @RequestMapping(value = "/confirm-account", method = RequestMethod.GET)
    public ResponseEntity<?> confirmAccountRegistration(
        @RequestParam("token") String confirmationToken) {

        ConfirmationToken token = confirmationTokenRepository
            .findByConfirmationToken(confirmationToken);

        if (token != null) {
            authService.setConfirmed(token);
            return ResponseEntity.ok("Akun berhasil dikonfirmasi");
        } else {
            return ResponseEntity.badRequest().body("Token yang diberikan tidak valid");
        }
    }

}
