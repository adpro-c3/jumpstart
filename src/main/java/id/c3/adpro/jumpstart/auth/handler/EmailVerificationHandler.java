package id.c3.adpro.jumpstart.auth.handler;

import id.c3.adpro.jumpstart.auth.config.JwtUtils;
import id.c3.adpro.jumpstart.auth.model.JwtRequest;
import id.c3.adpro.jumpstart.auth.service.UserDetailService;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class EmailVerificationHandler implements AuthHandler{

    private String token;
    private AuthHandler nextHandler;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private PenggunaRepository penggunaRepository;

    @Override
    public void handle(JwtRequest request) throws Exception {
        final String username = request.getUsername();

        final UserDetails userDetails = userDetailService.loadUserByUsername(username);

        if (penggunaRepository.findByEmail(username).isConfirmed()) {
            token = jwtUtils.generateToken(userDetails);
        } else {
            throw new Exception("Please verify your email address");
        }

    }

    public String getToken(){
        return token;
    }

    public void setNextHandler(AuthHandler handler){
        this.nextHandler = handler;
    }

    public AuthHandler getNextHandler() {
        return nextHandler;
    };
}
