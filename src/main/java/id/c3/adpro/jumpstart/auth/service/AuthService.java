package id.c3.adpro.jumpstart.auth.service;


import id.c3.adpro.jumpstart.auth.model.ConfirmationToken;
import id.c3.adpro.jumpstart.auth.model.JwtRequest;
import java.util.Map;

public interface AuthService {

    String getName(String username);

    boolean isAdmin(String username);

    Map<String, String> getUserData(String username, String token);

    void setConfirmed(ConfirmationToken token);

    Map<String,String> authenticate(JwtRequest request) throws Exception;
}
