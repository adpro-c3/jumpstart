package id.c3.adpro.jumpstart.auth.service;

import id.c3.adpro.jumpstart.auth.model.MailType;

public interface NotifyService {

    void notify(MailType mailType, String[] args) throws IllegalStateException;

    void ping();

}
