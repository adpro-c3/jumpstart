package id.c3.adpro.jumpstart.auth.service;

import id.c3.adpro.jumpstart.auth.handler.EmailVerificationHandler;
import id.c3.adpro.jumpstart.auth.handler.PasswordValidationHandler;
import id.c3.adpro.jumpstart.auth.model.ConfirmationToken;
import id.c3.adpro.jumpstart.auth.model.JwtRequest;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private PenggunaRepository penggunaRepository;

    @Autowired
    private PasswordValidationHandler passwordValidationHandler;

    @Autowired
    private EmailVerificationHandler emailVerificationHandler;

    @Override
    public String getName(String username) {
        return penggunaRepository.findByEmail(username).getNama();
    }

    @Override
    public boolean isAdmin(String username) {
        return penggunaRepository.findByEmail(username).isAdmin();
    }

    @Override
    public Map<String, String> getUserData(String username, String token) {
        Map<String, String> userJson = new HashMap<>();

        userJson.put("token", token);
        userJson.put("nama", getName(username));
        userJson.put("isAdmin", Boolean.toString(isAdmin(username)));

        return userJson;
    }

    @Override
    public void setConfirmed(ConfirmationToken token) {
        Pengguna pengguna = penggunaRepository.findByEmail(token.getPengguna().getEmail());
        pengguna.setConfirmed(true);
        penggunaRepository.save(pengguna);
    }

    @Override
    public Map<String,String> authenticate(JwtRequest request) throws Exception {
        passwordValidationHandler.setNextHandler(emailVerificationHandler);
        passwordValidationHandler.handle(request);
        return getUserData(request.getUsername(), emailVerificationHandler.getToken());
    }

}
