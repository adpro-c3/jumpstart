package id.c3.adpro.jumpstart.auth.handler;

import id.c3.adpro.jumpstart.auth.model.JwtRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

@Component
public class PasswordValidationHandler implements AuthHandler{

    private AuthHandler nextHandler;

    @Autowired
    private AuthenticationManager authenticationManager;

    public void setNextHandler(AuthHandler handler){
        this.nextHandler = handler;
    }

    public AuthHandler getNextHandler(){
        return nextHandler;
    }


    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @Override
    public void handle(JwtRequest request) throws Exception {
        boolean success = false;

        final String username = request.getUsername();
        final String password = request.getPassword();

        try {
            authenticate(username, password);
            success = true;
        } catch (Exception e) {
            throw new Exception("Password didn't match", e);
        }

        if (success){
            this.nextHandler.handle(request);
        }
    }
}
