package id.c3.adpro.jumpstart.auth.model;

public enum MailType {
    APPLICATION,
    RECRUITMENT,
    NOTIFY,
    CONFIRMATION
}
