package id.c3.adpro.jumpstart.auth.service;

import id.c3.adpro.jumpstart.auth.model.MailRequest;
import id.c3.adpro.jumpstart.auth.model.MailType;
import java.net.UnknownHostException;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class NotifyServiceImpl implements NotifyService {

    @Value("${notif.domain}")
    private String domain;
    private String baseUri;
    private final Logger LOG = LoggerFactory.getLogger(NotifyService.class);

    public void notify(MailType mailType, String[] args) throws IllegalStateException {

        MailRequest mail = new MailRequest(args[0], args[1], Arrays.copyOfRange(args, 2, args.length));
        if (baseUri == null || baseUri.equals("")) {
            baseUri = String.format("http://%s/", domain);
        }
        String uri = baseUri;
        switch (mailType) {
            case CONFIRMATION:
                uri += "confirmationMail";
                break;
            case NOTIFY:
                uri += "notifyMail";
                break;
            case RECRUITMENT:
                uri += "recruitmentMail";
                break;
            case APPLICATION:
                uri += "applicationMail";
                break;
        }
        WebClient
            .create(uri)
            .post()
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(mail), MailRequest.class)
            .retrieve()
            .bodyToMono(Void.class)
            .onErrorResume(e -> {
                if (e instanceof UnknownHostException) {
                    LOG.warn("Failed to reach host, make sure the host is correct");
                } else {
                    LOG.error("Failed to post a mail");
                    e.printStackTrace();
                }
                return Mono.empty();
            })
            .subscribe();
    }

    @Override
    public void ping() {
        if (baseUri == null || baseUri.equals("")) {
            baseUri = String.format("http://%s/", domain);
        }
        WebClient
            .create(baseUri + "ping")
            .get()
            .retrieve()
            .bodyToMono(Void.class)
            .onErrorResume(e -> {
                if (e instanceof UnknownHostException) {
                    LOG.warn("Failed to reach host, make sure the host is correct");
                } else {
                    LOG.error("Failed to ping mailer");
                    e.printStackTrace();
                }
                return Mono.empty();
            })
            .subscribe();
    }
}
