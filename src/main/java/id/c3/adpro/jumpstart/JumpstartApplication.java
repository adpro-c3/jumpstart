package id.c3.adpro.jumpstart;

import id.c3.adpro.jumpstart.auth.service.NotifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class JumpstartApplication {

    @Autowired
    private NotifyService notifyService;
    @Value("${notif.domain}")
    private String domain;
    private Logger LOG = LoggerFactory.getLogger(JumpstartApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(JumpstartApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void postStartup() {
        LOG.info("Pinging Notification Service on " + domain);
        notifyService.ping();
        LOG.info("Completed initialization");
    }

}
