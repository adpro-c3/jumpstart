package id.c3.adpro.jumpstart.core.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static Date today() {
        return Calendar.getInstance().getTime();
    }

    public static Date daysFrom(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }
}
