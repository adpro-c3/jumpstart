package id.c3.adpro.jumpstart.core.repository;

import id.c3.adpro.jumpstart.core.model.entities.Job;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.model.types.JobStatus;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {

    List<Job> findAllBypengguna(Pengguna pengguna);

    List<Job> findAllByStatusEquals(JobStatus status);

    Job findJobBySlug(String slug);

    void deleteJobBySlug(String slug);
}
