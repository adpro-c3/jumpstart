package id.c3.adpro.jumpstart.core.utils;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtil {

    public static String slugify(String s) {
        return Normalizer.normalize(s, Form.NFD)
            .replaceAll("[^\\w\\s-]", "")
            .trim()
            .replaceAll("\\s+", "-")
            .toLowerCase();
    }

    public static String truncate(String s) {
        return truncate(s, 10);
    }

    public static String truncate(String s, int length) {
        return s.substring(0, Math.min(s.length(), length));
    }

    public static String encodeSlug(String s, long ID, Date date) {
        s = truncate(s);
        return slugify(s + new SimpleDateFormat("-yymmddhhmmss").format(date) + ID);
    }

}
