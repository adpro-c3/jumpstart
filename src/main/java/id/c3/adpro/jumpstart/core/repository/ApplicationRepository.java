package id.c3.adpro.jumpstart.core.repository;

import id.c3.adpro.jumpstart.core.model.entities.Application;
import id.c3.adpro.jumpstart.core.model.entities.Job;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.model.types.ApplicationStatus;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {

    List<Application> findAllByJob(Job job);

    List<Application> findAllByPengguna(Pengguna pengguna);

    List<Application> findAllByStatus(ApplicationStatus status);

    Application findApplicationById(Long id);

    void deleteApplicationById(Long id);

}
