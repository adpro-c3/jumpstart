package id.c3.adpro.jumpstart.core.utils;

import java.util.Date;

public class Util {

    public static String slugify(String s, long id) {
        Date today = DateUtil.today();
        return StringUtil.encodeSlug(s, id, today);
    }

    public static Date dateAfterDays(int days) {
        return DateUtil.daysFrom(days);
    }

    public static Date dateToday() {
        return DateUtil.today();
    }
}
