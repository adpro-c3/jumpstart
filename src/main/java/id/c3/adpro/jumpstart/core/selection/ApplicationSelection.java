package id.c3.adpro.jumpstart.core.selection;

public class ApplicationSelection {

    private int[] accepted;
    private int[] rejected;

    public ApplicationSelection(int[] accepted, int[] rejected) {
        this.accepted = accepted;
        this.rejected = rejected;
    }

    public int[] getAccepted() {
        return this.accepted;
    }

    public int[] getRejected() {
        return this.rejected;
    }

}
