package id.c3.adpro.jumpstart.core.service;

import id.c3.adpro.jumpstart.core.model.entities.Pengguna;

public interface PenggunaService {

    Pengguna getUserProfile(String email);

    Pengguna updateUserProfile(String email, Pengguna user);
}
