package id.c3.adpro.jumpstart.core.controller;


import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.service.PenggunaService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/user")
@CrossOrigin(origins = "*")
public class PenggunaController {

    @Autowired
    PenggunaService userService;

    @Timed("getprofile")
    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getUserProfile(Authentication auth) {
        String email = auth.getName();
        return ResponseEntity.ok(userService.getUserProfile(email));
    }
    @Timed("updateprofile")
    @PutMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity UpdateUserProfile(Authentication auth, @RequestBody Pengguna user) {
        String email = auth.getName();
        return ResponseEntity.ok(userService.updateUserProfile(email, user));
    }

}
