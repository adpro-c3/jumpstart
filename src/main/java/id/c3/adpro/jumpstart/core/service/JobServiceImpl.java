package id.c3.adpro.jumpstart.core.service;

import id.c3.adpro.jumpstart.auth.model.MailType;
import id.c3.adpro.jumpstart.auth.service.NotifyService;
import id.c3.adpro.jumpstart.core.model.entities.Application;
import id.c3.adpro.jumpstart.core.model.entities.Job;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.model.types.ApplicationStatus;
import id.c3.adpro.jumpstart.core.model.types.JobStatus;
import id.c3.adpro.jumpstart.core.repository.ApplicationRepository;
import id.c3.adpro.jumpstart.core.repository.JobRepository;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import id.c3.adpro.jumpstart.core.selection.ApplicationSelection;
import id.c3.adpro.jumpstart.core.utils.Util;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class JobServiceImpl implements JobService {

    private final String ACCEPTED = "diterima";
    private final String REJECTED = "tidak diterima";

    @Autowired
    PenggunaRepository penggunaRepository;

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    JobRepository jobRepository;

    @Autowired
    NotifyService notifyService;

    @Override
    public Iterable<Job> getAvailableJobs() {
        Collection<Job> jobs = jobRepository.findAll();
        return clean(jobs);
    }

    @Override
    public Iterable<Job> getAvailableJobsByStatus(JobStatus status) {
        Collection<Job> jobs = jobRepository.findAllByStatusEquals(status);
        return clean(jobs);
    }

    @Override
    public Job registerNewJob(Job job, String emailUser) {
        Pengguna user = penggunaRepository.findByEmail(emailUser);
        job = jobRepository.save(job); // this is ok because JPA only register at the next flush
        String slug = Util.slugify(job.getNama(), job.getID());
        job.setSlug(slug);
        job.setPengguna(user);
        return jobRepository.save(job);
    }

    @Override
    public Job getJobBySlug(String slug) {
        Job job = jobRepository.findJobBySlug(slug);
        if (job == null) {
            return null;
        }
        return (notExpired(job)) ? job : null;
    }

    @Override
    public Job replaceJobBySlug(String slug, Job job) {
        Job old = jobRepository.findJobBySlug(slug);
        if (old == null) {
            return null;
        }
        job.setID(old.getID());
        job.setSlug(slug);
        return jobRepository.save(job);
    }

    @Override
    @Transactional
    public void deleteJobBySlug(String slug) {
        jobRepository.deleteJobBySlug(slug);
    }

    @Override
    public void closeJobBySlug(String slug) {
        Job selected = jobRepository.findJobBySlug(slug);
        if (selected != null) {
            selected.setStatus(JobStatus.CLOSED);
            jobRepository.save(selected);
        }
    }

    @Override
    public void applyJobBySlug(String slug, String userEmail) {
        Pengguna user = penggunaRepository.findByEmail(userEmail);
        Job job = jobRepository.findJobBySlug(slug);
        Application app = new Application();
        app.setJob(job);
        app.setPengguna(user);
        applicationRepository.save(app);
    }

    @Override
    public void verifyJobBySlug(String slug){
        Job selected = jobRepository.findJobBySlug(slug);
        if (selected != null) {
            ArrayList<String> recruiterData = new ArrayList<>();
            recruiterData.add(selected.getPengguna().getEmail());
            recruiterData.add(selected.getPengguna().getNama());

            if (selected.getStatus() == JobStatus.NOT_VERIFIED) {
                selected.setStatus(JobStatus.VERIFIED);
                recruiterData.add("true");

            } else recruiterData.add("false");
            notifyService.notify(MailType.RECRUITMENT, recruiterData.toArray(new String[]{}));

            jobRepository.save(selected);
            if (penggunaRepository.findAll() != null){
                for (Pengguna pengguna:penggunaRepository.findAll()) {
                    if (!pengguna.getEmail().equals(selected.getPengguna().getEmail()) &&
                        pengguna.getSkills() != null &&
                        pengguna.getSkills().contains(selected.getPosisi()))
                    {
                        notifyService.notify(MailType.NOTIFY,
                            new String[]{pengguna.getEmail(),pengguna.getNama(),
                                selected.getNama(), selected.getPosisi()});
                    }
                }
            }

        }
    }

    private boolean notExpired(Job job) {
        return job.getExpiry().compareTo(Util.dateToday()) > 0;
    }

    private Iterable<Job> clean(Collection<Job> jobs) {
        jobs.removeIf(job -> !notExpired(job));
        return jobs;
    }

    @Override
    public void processApplication(ApplicationSelection selection) {
        for (int i : selection.getAccepted()) {
            Application application = applicationRepository.findApplicationById((long) i);
            if (application.getStatus() == ApplicationStatus.WAITING) {
                application.setStatus(ApplicationStatus.ACCEPTED);
                String[] acceptedApplicantData = new String[]{
                    application.getPengguna().getEmail(),
                    application.getPengguna().getNama(),
                    application.getjob_name(),
                    ACCEPTED
                };
                notifyService.notify(MailType.APPLICATION, acceptedApplicantData);
            }
            applicationRepository.save(application);
        }
        for (int i : selection.getRejected()) {
            Application application = applicationRepository.findApplicationById((long) i);
            if (application.getStatus() == ApplicationStatus.WAITING) {
                application.setStatus(ApplicationStatus.REJECTED);
                String[] rejectedApplicantData = new String[]{
                    application.getPengguna().getEmail(),
                    application.getPengguna().getNama(),
                    application.getjob_name(),
                    REJECTED
                };
                notifyService.notify(MailType.APPLICATION, rejectedApplicantData);
            }
            applicationRepository.save(application);
        }
    }
}
