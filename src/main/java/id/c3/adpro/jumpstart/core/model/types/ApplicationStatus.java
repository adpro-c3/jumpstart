package id.c3.adpro.jumpstart.core.model.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ApplicationStatus {
    @JsonProperty("waiting") WAITING,
    @JsonProperty("accepted") ACCEPTED,
    @JsonProperty("rejected") REJECTED
}
