package id.c3.adpro.jumpstart.core.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.c3.adpro.jumpstart.core.model.types.ApplicationStatus;
import id.c3.adpro.jumpstart.core.model.types.JobStatus;
import id.c3.adpro.jumpstart.core.model.types.JobType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Entity  // This tells Hibernate to make a table out of this class
@Data // Lombok: adds getters and setters
@NoArgsConstructor //Lombok: creates a noargsconstructor
@Table(name = "applications")
public class Application {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column
    private Long id;

    @ManyToOne
    @JoinColumn(name = "job", referencedColumnName = "id")
    @JsonIgnore
    @ToString.Exclude
    private Job job;


    @ManyToOne
    @JoinColumn(name = "pengguna_email", referencedColumnName = "email")
    @JsonIgnore
    private Pengguna pengguna;

    @Column
    private ApplicationStatus status = ApplicationStatus.WAITING;

    public String getjob_name() {
        return job.getNama();
    }

    public String getjob_posisi() {
        return job.getPosisi();
    }

    public JobType getjob_type() {
        return job.getType();
    }

    public JobStatus getjob_status() {
        return job.getStatus();
    }

    public String getuser_nama() {
        return pengguna.getNama();
    }

    public String getuser_portfolio() {
        return pengguna.getPortofolio();
    }

}
