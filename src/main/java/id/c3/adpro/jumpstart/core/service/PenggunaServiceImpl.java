package id.c3.adpro.jumpstart.core.service;


import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PenggunaServiceImpl implements PenggunaService {

    @Autowired
    PenggunaRepository penggunaRepository;

    @Override
    public Pengguna getUserProfile(String email) {
        return penggunaRepository.findByEmail(email);
    }


    @Override
    public Pengguna updateUserProfile(String email, Pengguna user) {
        Pengguna oldUser = penggunaRepository.findByEmail(email);
        user.setEmail(email);
        user.setAdmin(oldUser.isAdmin());
        user.setPass(oldUser.getPass());
        penggunaRepository.save(user);
        return user;
    }
}
