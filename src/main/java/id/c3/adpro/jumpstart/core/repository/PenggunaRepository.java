package id.c3.adpro.jumpstart.core.repository;

import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PenggunaRepository extends JpaRepository<Pengguna, String> {

    Pengguna findByEmail(String email);
}
