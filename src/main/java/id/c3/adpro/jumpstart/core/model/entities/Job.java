package id.c3.adpro.jumpstart.core.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.c3.adpro.jumpstart.core.model.types.JobStatus;
import id.c3.adpro.jumpstart.core.model.types.JobType;
import id.c3.adpro.jumpstart.core.utils.Util;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Entity
@Data
@NoArgsConstructor
@Table(name = "jobs")
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column
    private long ID;
    @Column
    private String nama;
    @Column
    private String posisi;
    @Column(columnDefinition = "TEXT")
    private String description;
    @JsonProperty
    @Column
    private JobStatus status = JobStatus.NOT_VERIFIED;
    @Column
    private JobType type;
    @Column
    private String slug;
    @Column
    private Date expiry = Util.dateAfterDays(30);
    @ToString.Exclude
    @OneToMany(mappedBy = "job", targetEntity = Application.class,
        fetch = FetchType.EAGER)
    private List<Application> application;
    @ManyToOne
    @JoinColumn(name = "pengguna_email", referencedColumnName = "email")
    @JsonIgnore
    private Pengguna pengguna;

    public Job(Long ID, String nama, String posisi, String description, JobType type) {
        this.ID = ID;
        this.nama = nama;
        this.posisi = posisi;
        this.description = description;
        this.type = type;
        this.slug = Util.slugify(this.nama, this.ID);
    }

    public int getjob_applicants() {
        return (application != null) ? application.size() : 0;
    }
}
