package id.c3.adpro.jumpstart.core.controller;

import id.c3.adpro.jumpstart.core.model.entities.Job;
import id.c3.adpro.jumpstart.core.model.types.JobStatus;
import id.c3.adpro.jumpstart.core.selection.ApplicationSelection;
import id.c3.adpro.jumpstart.core.service.JobService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin
@RestController
@RequestMapping(path = "/job")
public class JobController {

    @Autowired
    private JobService jobService;

    @Timed("getavailablejobs")
    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Job>> getAvailableJobs(
        @RequestParam(value = "status", required = false) JobStatus status,
        @RequestParam(value = "skills", required = false) Iterable<String> skills,
        @RequestParam(value = "types", required = false) Iterable<String> types
    ) {
        Iterable<Job> jobs;

        try {
            if (status != null) {
                jobs = jobService.getAvailableJobsByStatus(status);
            } else {
                jobs = jobService.getAvailableJobs();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(jobs);
    }
    @Timed("registernewjob")
    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Job> registerNewJob(@RequestBody Job job, Authentication auth) {
        return ResponseEntity.ok(jobService.registerNewJob(job, auth.getName()));
    }
    @Timed("getjob")
    @GetMapping(path = "/{slug}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Job> getJobBySlug(@PathVariable String slug) {
        Job fetched = jobService.getJobBySlug(slug);
        if (fetched == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(fetched);
    }
    @Timed("replacejob")
    @PutMapping(path = "/{slug}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Job> replaceJobBySlug(@PathVariable String slug, @RequestBody Job job) {
        Job replaced = jobService.replaceJobBySlug(slug, job);
        if (replaced == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(replaced);
    }
    @Timed("deletejob")
    @DeleteMapping(path = "/{slug}", produces = {"application/json"})
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteJobBySlug(@PathVariable String slug) {
        jobService.deleteJobBySlug(slug);
    }
    @Timed("closejob")
    @PostMapping(path = "/{slug}/close", produces = {"application/json"})
    @ResponseStatus(code = HttpStatus.OK)
    public void closeJobBySlug(@PathVariable String slug) {
        jobService.closeJobBySlug(slug);
    }
    @Timed("applyjob")
    @PostMapping(path = "/{slug}/apply", produces = {"application/json"})
    @ResponseBody
    public void applyJobBySlug(@PathVariable String slug, Authentication auth) {
        // TODO: only return success/fail
        jobService.applyJobBySlug(slug, auth.getName());
//        return ResponseEntity.ok(slug);
    }
    @Timed("verifyjob")
    @PostMapping(path = "/{slug}/verify", produces = {"application/json"})
    @ResponseStatus(code = HttpStatus.OK)
    public void verifyJobBySlug(@PathVariable String slug){
        jobService.verifyJobBySlug(slug);
    }
    @Timed("processapplication")
    @PostMapping(path = "/process-application", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity processApplicationsByJob(@RequestBody ApplicationSelection selected) {
        jobService.processApplication(selected);
        return new ResponseEntity(HttpStatus.OK);
    }
}
