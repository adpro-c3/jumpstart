package id.c3.adpro.jumpstart.core.service;

import id.c3.adpro.jumpstart.core.model.entities.Job;
import id.c3.adpro.jumpstart.core.model.types.JobStatus;
import id.c3.adpro.jumpstart.core.selection.ApplicationSelection;

public interface JobService {

    Iterable<Job> getAvailableJobs();

    Iterable<Job> getAvailableJobsByStatus(JobStatus status);

    Job registerNewJob(Job job, String emailUser);

    Job getJobBySlug(String slug);

    Job replaceJobBySlug(String slug, Job job);

    void deleteJobBySlug(String slug);

    void closeJobBySlug(String slug);

    void applyJobBySlug(String slug, String userEmail);

    void verifyJobBySlug(String slug);

    void processApplication(ApplicationSelection selection);
}
