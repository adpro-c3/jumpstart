package id.c3.adpro.jumpstart.core.model.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum JobType {
    @JsonProperty("full_time") FULL_TIME,
    @JsonProperty("part_time") PART_TIME,
    @JsonProperty("freelance") FREELANCE
}
