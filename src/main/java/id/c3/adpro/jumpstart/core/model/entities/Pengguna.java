package id.c3.adpro.jumpstart.core.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity  // This tells Hibernate to make a table out of this class
@Data // Lombok: adds getters and setters
@NoArgsConstructor //Lombok: creates a noargsconstructor
@Table(name = "pengguna")
public class Pengguna {

    @Id
    @Column
    private String email;
    @Column
    @JsonIgnore
    private String pass;
    @Column
    private String biografi;
    @Column
    private String nama;
    @Column
    private String alamat;
    @Column
    private boolean isAdmin;
    @Column
    private String linkedIn;
    @Column
    private String github;
    @Column(columnDefinition = "TEXT")
    private String skills;
    @Column
    private String portofolio;
    @Column
    private boolean isConfirmed;
    @OneToMany(mappedBy = "pengguna", targetEntity = Application.class,
        fetch = FetchType.EAGER)
    private List<Application> applications;
    @OneToMany(mappedBy = "pengguna", targetEntity = Job.class,
        fetch = FetchType.EAGER)
    private List<Job> jobs;

    public Pengguna(
        String email,
        String pass,
        String nama,
        String alamat,
        String linkedIn,
        String github,
        String skills,
        String portofolio,
        String biografi,
        boolean isAdmin
    ) {
        this.email = email;
        this.pass = pass;
        this.nama = nama;
        this.alamat = alamat;
        this.linkedIn = linkedIn;
        this.github = github;
        this.skills = skills;
        this.portofolio = portofolio;
        this.isAdmin = isAdmin;
        this.biografi = biografi;
    }

    public Pengguna(String email, String passwd) {
        this.email = email;
        this.pass = passwd;
        this.isAdmin = false;
    }

    @JsonIgnore
    public String getPassword() {
        return this.pass;
    }

    @JsonIgnore
    public String getUsername() {
        return this.email;
    }

}
