package id.c3.adpro.jumpstart.core.model.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum JobStatus {
    @JsonProperty("not_verified") NOT_VERIFIED,
    @JsonProperty("verified") VERIFIED,
    @JsonProperty("closed") CLOSED

}
