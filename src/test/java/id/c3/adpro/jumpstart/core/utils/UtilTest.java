package id.c3.adpro.jumpstart.core.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Calendar;
import java.util.Date;
import org.junit.jupiter.api.Test;

class UtilTest {

    @Test
    public void testSlugCreationWithDateTest() {
        String slug = Util.slugify("hElLo", 9);
        assertTrue(slug.contains("hello-"));
        assertTrue(slug.endsWith("9"));
    }

    @Test
    public void testSlugTruncation() {
        String slug = Util.slugify("aabbccddeeff", 69);
        assertTrue(slug.contains("aabbccddee-"));
        assertTrue(slug.endsWith("69"));
    }

    @Test
    public void testTodayDateisReallyToday() {
        Date result = Util.dateToday();
        assertEquals(Calendar.DAY_OF_WEEK,
            org.assertj.core.util.DateUtil.toCalendar(result).DAY_OF_WEEK);
    }

    @Test
    public void testUtilClassCallable() {
        Util result = new Util();
        assertNotNull(result);
    }

    @Test
    public void testStringUtilClassCallable() {
        StringUtil result = new StringUtil();
        assertNotNull(result);
    }

    @Test
    public void testDateUtilClassCallable() {
        DateUtil result = new DateUtil();
        assertNotNull(result);
    }
}