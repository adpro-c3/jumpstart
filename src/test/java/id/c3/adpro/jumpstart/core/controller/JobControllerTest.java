package id.c3.adpro.jumpstart.core.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.c3.adpro.jumpstart.core.model.entities.Job;
import id.c3.adpro.jumpstart.core.model.types.JobStatus;
import id.c3.adpro.jumpstart.core.model.types.JobType;
import id.c3.adpro.jumpstart.core.selection.ApplicationSelection;
import id.c3.adpro.jumpstart.core.service.JobService;
import javax.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class JobControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private JobService jobService;

    private Job job;

    @BeforeEach
    void setUp() {
        job = new Job(
            1L,
            "dummy job",
            "dummy position",
            "dummy description",
            JobType.FREELANCE
        );

        mvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Test
    public void testGetAllJobsEndpointReturnsOkStatus() throws Exception {
        mvc.perform(get("/job").contentType("application/json")).andExpect(status().isOk());
    }

    @Test
    public void testGetAllJobsWithStatusEndpointReturnsOkStatus() throws Exception {
        mvc.perform(
            get("/job")
                .queryParam("status", String.valueOf(JobStatus.CLOSED))
                .contentType("application/json")

        ).andExpect(status().isOk());
    }

    @Test
    public void testGetAllJobsWithExceptionEndpointReturnsBadRequestStatus() throws Exception {
        when(jobService.getAvailableJobs()).thenThrow(EntityNotFoundException.class);
        mvc.perform(
            get("/job")
                .contentType("application/json")

        ).andExpect(status().isBadRequest());
    }

    @Test
    public void testGetSpecificJobsEndpointReturnsNotFoundStatus() throws Exception {
        mvc.perform(
            get("/job/{slug}", job.getSlug()).contentType("application/json")
        ).andExpect(status().isNotFound());
    }

    @Test
    public void testGetSpecificJobsEndpointReturnsOkStatus() throws Exception {
        when(jobService.getJobBySlug(anyString())).thenReturn(job);
        mvc.perform(
            get("/job/{slug}", job.getSlug()).contentType("application/json")
        ).andExpect(status().isOk());
    }

    @WithMockUser("dummy")
    @Test
    public void testPostAJobsEndpointReturnsOkStatus() throws Exception {
        mvc.perform(
            post("/job")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(job))
        ).andExpect(status().isOk());
    }

    @WithMockUser("dummy")
    @Test
    public void testPutAJobEndpointReturnsOkStatus() throws Exception {
        when(jobService.replaceJobBySlug(anyString(), any())).thenReturn(job);
        mvc.perform(
            put("/job/{slug}", job.getSlug())
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(job))
        ).andExpect(status().isOk());
    }

    @WithMockUser("dummy")
    @Test
    public void testPutAJobEndpointReturnsNotFoundStatus() throws Exception {
        mvc.perform(
            put("/job/{slug}", "non-existent-slug")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(job))
        ).andExpect(status().isNotFound());
    }

    @WithMockUser("dummy")
    @Test
    public void testDeleteAJobEndpointReturnsOkStatus() throws Exception {
        mvc.perform(
            delete("/job/{slug}", job.getSlug())
                .contentType("application/json")
        ).andExpect(status().isOk());
    }

    @WithMockUser("dummy")
    @Test
    public void testCloseAJobEndpointReturnsOkStatus() throws Exception {
        mvc.perform(
            post("/job/{slug}/close", job.getSlug())
                .contentType("application/json")
        ).andExpect(status().isOk());
    }

    @WithMockUser("dummy")
    @Test
    public void testApplyAJobEndpointReturnsOkStatus() throws Exception {
        mvc.perform(
            post("/job/{slug}/apply", job.getSlug())
                .contentType("application/json")
        ).andExpect(status().isOk());
    }

    @WithMockUser("dummy")
    @Test
    public void testVerifyAJobEndpointReturnsOkStatus() throws Exception {
        mvc.perform(
            post("/job/{slug}/verify", job.getSlug())
                .contentType("application/json")

        ).andExpect(status().isOk());
    }

    @WithMockUser("dummy")
    @Test
    public void testProcessApplicationEndpointReturnsOkStatus() throws Exception {
        int[] acc = {};
        int[] rej = {};
        ApplicationSelection app = new ApplicationSelection(acc, rej);
        mvc.perform(
            post("/job/process-application")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(app))
        ).andExpect(status().isOk());
        ;
    }

}