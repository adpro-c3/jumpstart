package id.c3.adpro.jumpstart.core.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.c3.adpro.jumpstart.auth.model.MailType;
import id.c3.adpro.jumpstart.auth.service.NotifyServiceImpl;
import id.c3.adpro.jumpstart.core.model.entities.Application;
import id.c3.adpro.jumpstart.core.model.entities.Job;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.model.types.ApplicationStatus;
import id.c3.adpro.jumpstart.core.model.types.JobStatus;
import id.c3.adpro.jumpstart.core.model.types.JobType;
import id.c3.adpro.jumpstart.core.repository.ApplicationRepository;
import id.c3.adpro.jumpstart.core.repository.JobRepository;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import id.c3.adpro.jumpstart.core.selection.ApplicationSelection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.StreamSupport;
import javax.annotation.Resource;
import org.assertj.core.util.DateUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

//anti stubbing (looses strict stubbing)


@ExtendWith(MockitoExtension.class)
public class JobServiceTest {

    @Mock
    private JobRepository jobRepository;

    @Mock
    private PenggunaRepository penggunaRepository;

    @Mock
    private ApplicationRepository applicationRepository;

    @InjectMocks
    private JobServiceImpl jobService;

    @InjectMocks
    private PenggunaServiceImpl penggunaService;

    @Autowired
    private NotifyServiceImpl notifyService;

    private Job job, job2, job3;

    private Application app, application, application2;

    private Pengguna user, user2;

    private String username,nama;

    @BeforeEach
    void setUp() {

        job = new Job(1234L, "Gimdep", "Programmer", "We need coders for minecraft",
            JobType.FREELANCE);

        app = new Application();
        applicationRepository.save(app);
        List<Application> listApp = new ArrayList<>();
        listApp.add(app);
        app.setId(555L);
        app.setJob(job);
        job.setApplication(listApp);

        user = new Pengguna(
            "dummy",
            "password",
            "dummy",
            "depok",
            "linkedin",
            "github",
            "SQL, Programmer",
            "portfolio",
            "bio",
            false);
        penggunaRepository.save(user);
        user2 = spy(user);
        lenient().when(penggunaService.getUserProfile("dummy")).thenReturn(user);

        ReflectionTestUtils.setField(jobService, "notifyService", new NotifyServiceImpl());

        job2 = new Job(1235L, "Webdev", "Programmer", "We need coders for minecraft",
            JobType.FREELANCE);
        job3 = spy(job);
        application = new Application();
        application.setPengguna(user);
        application.setJob(job);
        application.setStatus(ApplicationStatus.WAITING);
        application.setId(1L);
        applicationRepository.save(application);

        application2 = new Application();
        application2.setPengguna(user);
        application2.setJob(job2);
        application2.setStatus(ApplicationStatus.WAITING);
        application2.setId(2L);
        applicationRepository.save(application2);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testServiceRegisterNewJob() throws Exception {
        when(jobRepository.save(any())).thenReturn(job);
        jobService.registerNewJob(job, "dummy");
        verify(jobRepository, times(2)).save(any());
    }

    @Test
    public void testServiceGetAvailableJobs() throws Exception {
        jobService.getAvailableJobs();
        verify(jobRepository, times(1)).findAll();
    }

    @Test
    public void testServiceGetAvailableJobsByStatus() throws Exception {
        jobService.getAvailableJobsByStatus(JobStatus.NOT_VERIFIED);
        verify(jobRepository, times(1)).findAllByStatusEquals(JobStatus.NOT_VERIFIED);
    }

    @Test
    public void testServiceGetJobBySlug() throws Exception {
        when(jobRepository.findJobBySlug(anyString())).thenReturn(job);
        jobService.getJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).findJobBySlug(job.getSlug());
    }

    @Test
    public void testServiceGetJobBySlugButNotFound() throws Exception {
        jobService.getJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).findJobBySlug(job.getSlug());
    }

    @Test
    public void testServiceGetJobBySlugButExpired() throws Exception {
        job.setExpiry(DateUtil.yesterday());
        when(jobRepository.findJobBySlug(anyString())).thenReturn(job);
        Job result = jobService.getJobBySlug(job.getSlug());
        assertNull(result);
    }

    @Test
    public void testServiceVerifyJobBySlug() throws Exception {
        when(jobRepository.save(any())).thenReturn(job);
        when(jobRepository.findJobBySlug(any())).thenReturn(job3);
        when(penggunaRepository.findAll()).thenReturn(new ArrayList<Pengguna>(Arrays.asList(user2)));
        when(job3.getPengguna()).thenReturn(user2);
        when(user2.getNama()).thenReturn("nama");
        when(user2.getEmail()).thenReturn("email");
        jobService.verifyJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).findJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).save(any());
        verify(penggunaRepository,times(2)).findAll();
    }

    @Test
    public void testServiceVerifyJobBySlugButNotFound() throws Exception {
        jobService.verifyJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).findJobBySlug(job.getSlug());
        verify(jobRepository, times(0)).save(any());
    }

    @Test
    public void testServiceVerifyJobBySlugStatusChanged() throws Exception {
        when(jobRepository.save(any())).thenReturn(job3);
        when(jobRepository.findJobBySlug(any())).thenReturn(job3);
        when(penggunaRepository.findAll()).thenReturn(new ArrayList<Pengguna>(Arrays.asList(user2)));
        assertEquals(job3.getStatus(), JobStatus.NOT_VERIFIED);
        when(job3.getPengguna()).thenReturn(user2);
        when(user2.getEmail()).thenReturn("email");
        when(user2.getNama()).thenReturn("nama");

        jobService.verifyJobBySlug(job.getSlug());
        assertEquals(JobStatus.VERIFIED, job3.getStatus());
        verify(jobRepository, times(1)).findJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).save(any());
        verify(penggunaRepository,times(2)).findAll();
    }

    @Test
    public void testServiceCloseJobBySlug() throws Exception {
        when(jobRepository.save(any())).thenReturn(job);
        when(jobRepository.findJobBySlug(any())).thenReturn(job);
        jobService.closeJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).findJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).save(any());
    }

    @Test
    public void testServiceCloseJobBySlugButNotFound() throws Exception {
        jobService.closeJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).findJobBySlug(job.getSlug());
        verify(jobRepository, times(0)).save(any());
    }

    @Test
    public void testServiceApplyJobBySlug() throws Exception {
        jobService.applyJobBySlug(job.getSlug(), "dummy");
    }

    @Test
    public void testServiceReplaceJobBySlug() throws Exception {
        when(jobRepository.findJobBySlug(any())).thenReturn(null);
        Job result = jobService.replaceJobBySlug(job.getSlug(), job);
        assertNull(result);
        verify(jobRepository, times(1)).findJobBySlug(job.getSlug());
    }

    @Test
    public void testServiceReplaceJobBySlugNotFoundReturnsNull() throws Exception {
        when(jobRepository.save(any())).thenReturn(job);
        when(jobRepository.findJobBySlug(any())).thenReturn(job);
        jobService.replaceJobBySlug(job.getSlug(), job);
        verify(jobRepository, times(1)).findJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).save(any());
    }

    @Test
    public void testServiceDeleteJobBySlug() throws Exception {
        jobService.deleteJobBySlug(job.getSlug());
        verify(jobRepository, times(1)).deleteJobBySlug(job.getSlug());
    }

    @Test
    public void testServiceCleanerExpired() {
        List<Job> jobs = new ArrayList<>();
        job.setExpiry(DateUtil.yesterday());
        jobs.add(job);

        when(jobRepository.findAll()).thenReturn(jobs);
        Iterable<Job> result = jobService.getAvailableJobs();
        assertEquals(StreamSupport.stream(result.spliterator(), false).count(), 0);
    }

    @Test
    public void testServiceCleanerNotExpired() {
        List<Job> jobs = new ArrayList<>();
        jobs.add(job);

        when(jobRepository.findAll()).thenReturn(jobs);
        Iterable<Job> result = jobService.getAvailableJobs();
        assertEquals(StreamSupport.stream(result.spliterator(), false).count(), 1);
    }

    @Test
    public void testServiceProcessApplication() throws Exception {
        int[] acc = {1};
        int[] rej = {2};
        ApplicationSelection selection = new ApplicationSelection(acc, rej);
        when(applicationRepository.save(application)).thenReturn(application);
        when(applicationRepository.save(application2)).thenReturn(application2);
        when(applicationRepository.findApplicationById(1L)).thenReturn(application);
        when(applicationRepository.findApplicationById(2L)).thenReturn(application2);
        jobService.processApplication(selection);
        verify(applicationRepository, times(1)).findApplicationById(1L);
    }
}

