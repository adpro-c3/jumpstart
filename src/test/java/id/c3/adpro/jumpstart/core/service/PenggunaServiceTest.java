package id.c3.adpro.jumpstart.core.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;

import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PenggunaServiceTest {

    @Mock
    private PenggunaRepository penggunaRepository;

    @InjectMocks
    private PenggunaServiceImpl penggunaService;

    private Pengguna user;

    @BeforeEach
    public void setUp() {
        user = new Pengguna(
            "althof@gmail.com",
            "password",
            "althof",
            "depok",
            "linkedin",
            "github",
            "SQL",
            "portfolio",
            "bio",
            false);
        penggunaRepository.save(user);
        lenient().when(penggunaService.getUserProfile("althof@gmail.com")).thenReturn(user);
    }

    @Test
    public void testServiceGetUserProfile() {

        Pengguna resultUser = penggunaService.getUserProfile(user.getEmail());
        assertEquals(user.getEmail(), resultUser.getEmail());
    }

    @Test
    public void testServiceUpdateUserProfile() {

        String currentNama = user.getNama();
        user.setNama("new nama");

        lenient().when(penggunaService.updateUserProfile(user.getEmail(), user))
            .thenReturn(user);
        Pengguna resultUser = penggunaService.updateUserProfile(user.getEmail(), user);

        assertNotEquals(resultUser.getNama(), currentNama);
        assertEquals(resultUser.getNama(), user.getNama());
    }


}
