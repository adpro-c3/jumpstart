package id.c3.adpro.jumpstart.core.controller;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.service.PenggunaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PenggunaControllerTest {

    Pengguna pengguna;
    @MockBean
    private PenggunaServiceImpl penggunaService;
    @Autowired
    private WebApplicationContext context;
    private MockMvc mvc;

    @BeforeEach
    public void setup() {
        pengguna = new Pengguna(
            "althof@gmail.com",
            "password",
            "althof",
            "depok",
            "linkedin",
            "github",
            "SQL",
            "portfolio",
            "bio",
            false);

        mvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    private String mapToJson(Pengguna obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @WithMockUser("althof@gmail.com")
    @Test
    public void testControllerGetProfile() throws Exception {

        when(penggunaService.getUserProfile("althof@gmail.com"))
            .thenReturn(pengguna);

        mvc.perform(get("/user").contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk());
    }

    @WithMockUser("althof@gmail.com")
    @Test
    public void testUpdateProfile() throws Exception {

        pengguna.setNama("BUdi");

        when(penggunaService.updateUserProfile(pengguna.getEmail(), pengguna))
            .thenReturn(pengguna);

        mvc.perform(put("/user").contentType(MediaType.APPLICATION_JSON)
            .content(mapToJson(pengguna)))
            .andExpect(status().isOk());


    }

}
