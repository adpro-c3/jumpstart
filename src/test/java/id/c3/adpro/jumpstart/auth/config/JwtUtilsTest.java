package id.c3.adpro.jumpstart.auth.config;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;

import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import java.util.ArrayList;
import org.assertj.core.util.DateUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
public class JwtUtilsTest {

    private final String PENGGUNA_EMAIL = "real@mail.com";
    private final String PENGGUNA_PWD = "realpwd";
    @Spy
    @InjectMocks
    private JwtUtils jwtUtils;
    private Pengguna pengguna;
    private UserDetails correctPengguna;
    private UserDetails incorrectPengguna;

    @BeforeEach
    public void setUp() {
        ReflectionTestUtils.setField(jwtUtils, "secretKey", "TK_C3_SK");
        correctPengguna = new User(PENGGUNA_EMAIL, PENGGUNA_PWD, new ArrayList<>());
        incorrectPengguna = new User("fake@mail.com", "fakepwd", new ArrayList<>());
    }

    @Test
    public void testGenerateTokenAndValidateMatchTokenShouldSuccess() {
        String token = jwtUtils.generateToken(correctPengguna);
        assertTrue(jwtUtils.validateToken(token, correctPengguna));
    }

    @Test
    public void testGenerateTokenAndValidateIncorrectUserShouldFail() {
        String token = jwtUtils.generateToken(correctPengguna);
        assertFalse(jwtUtils.validateToken(token, incorrectPengguna));
    }

    @Test
    public void testExpiredTokenShouldFail() {
        String token = jwtUtils.generateToken(correctPengguna);
        doReturn(DateUtil.yesterday()).when(jwtUtils).getExpirationDateFromToken(anyString());
        assertFalse(jwtUtils.validateToken(token, correctPengguna));
    }


}
