package id.c3.adpro.jumpstart.auth.model;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JwtResponseTest {

    private JwtResponse jwtResponse;
    private String DUMMY_TOKEN = "dummytoken";

    @BeforeEach
    public void setUp() {
        jwtResponse = new JwtResponse(DUMMY_TOKEN);
    }

    @Test
    public void testGetTokenShouldReturnToken() {
        assertEquals(DUMMY_TOKEN, jwtResponse.getToken());
    }
}
