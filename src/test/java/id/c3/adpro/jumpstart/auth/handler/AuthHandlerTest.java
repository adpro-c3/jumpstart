package id.c3.adpro.jumpstart.auth.handler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.c3.adpro.jumpstart.auth.config.JwtUtils;
import id.c3.adpro.jumpstart.auth.model.JwtRequest;
import id.c3.adpro.jumpstart.auth.service.AuthServiceImpl;
import id.c3.adpro.jumpstart.auth.service.UserDetailService;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import java.util.LinkedList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@ExtendWith(MockitoExtension.class)
public class AuthHandlerTest {

    final private String USERNAME = "regis.jumpstart@gmail.com";
    final private String PASSWORD = "PWD";
    final private String NAMA = "NAMA";
    final private String DUMMY_TOKEN = "DUMMYTOKEN";
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private PenggunaRepository penggunaRepository;
    @InjectMocks
    private EmailVerificationHandler emailVerificationHandler;
    @InjectMocks
    private PasswordValidationHandler passwordValidationHandler;
    @MockBean
    private AuthServiceImpl authService;
    @Mock
    private UserDetailService userDetailService;
    @Mock
    private JwtUtils jwtUtils;
    private JwtRequest jwtRequest;
    private Pengguna pengguna;
    private UserDetails penggunaUser;

    @BeforeEach
    public void setUp() {
        jwtRequest = new JwtRequest(USERNAME, PASSWORD);
        pengguna = new Pengguna(USERNAME, PASSWORD);
        penggunaUser = new User(USERNAME, PASSWORD, new LinkedList<>());
    }

    @Test
    public void testPasswordValidationHandlerSetNextHandler() {
        passwordValidationHandler.setNextHandler(emailVerificationHandler);
        Assertions.assertNotNull(passwordValidationHandler.getNextHandler());
    }

    @Test
    public void testEmailVerificationHandlerSetNextHandler() {
        emailVerificationHandler.setNextHandler(emailVerificationHandler);
        Assertions.assertNotNull(emailVerificationHandler.getNextHandler());
    }

    @Test
    public void testAuthHandlerlerHandle() throws Exception {
        passwordValidationHandler.setNextHandler(emailVerificationHandler);
        when(authenticationManager.authenticate(any())).thenReturn(null);
        when(penggunaRepository.findByEmail(anyString())).thenReturn(pengguna);
        when(userDetailService.loadUserByUsername(anyString())).thenReturn(penggunaUser);
        when(penggunaRepository.findByEmail(anyString())).thenReturn(pengguna);
        when(jwtUtils.generateToken(any())).thenReturn("Token");
        pengguna.setConfirmed(true);
        passwordValidationHandler.handle(jwtRequest);
        String resultToken = emailVerificationHandler.getToken();
        Assertions.assertEquals("Token", resultToken);
        verify(authenticationManager, times(1)).authenticate(any());
    }

    @Test
    public void testEmailVerificationHandlerThrowExceptionWhenEmailNotYetVerified() {
        Exception exception = Assertions.assertThrows(Exception.class,  () -> {
            passwordValidationHandler.setNextHandler(emailVerificationHandler);
            when(authenticationManager.authenticate(any())).thenReturn(null);
            when(penggunaRepository.findByEmail(anyString())).thenReturn(pengguna);
            when(userDetailService.loadUserByUsername(anyString())).thenReturn(penggunaUser);
            passwordValidationHandler.handle(jwtRequest);
        });
        String expected = "Please verify your email address";
        String result = exception.getMessage();
        Assertions.assertEquals(expected,result);
    }

    @Test
    public void testPasswordValidationHandlerHandleThrowsExceptionWhenPasswordDidntMatch() {
        Exception exception = Assertions.assertThrows(Exception.class,  () -> {
            passwordValidationHandler.setNextHandler(emailVerificationHandler);
            when(authenticationManager.authenticate(any())).thenThrow(BadCredentialsException.class);
            passwordValidationHandler.handle(jwtRequest);
        });
        String expected = "Password didn't match";
        String result = exception.getMessage();
        Assertions.assertEquals(expected,result);
    }

}
