package id.c3.adpro.jumpstart.auth.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@ExtendWith(MockitoExtension.class)
public class UserDetailServiceTest {

    private final String NON_EXISTENT_EMAIL = "fake@mail.com";
    private final String PENGGUNA_EMAIL = "real@mail.com";
    private final String PENGGUNA_PWD = "realpwd";
    @InjectMocks
    UserDetailService userDetailService;
    @Mock
    PenggunaRepository penggunaRepository;
    private Pengguna pengguna;

    @BeforeEach
    public void setUp() {
        pengguna = new Pengguna(PENGGUNA_EMAIL, PENGGUNA_PWD);
    }

    @Test
    public void testLoadUserByExistUsernameShouldSucceed() {
        when(penggunaRepository.findByEmail(PENGGUNA_EMAIL)).thenReturn(pengguna);

        UserDetails result = userDetailService.loadUserByUsername(pengguna.getUsername());

        assertEquals(PENGGUNA_EMAIL, result.getUsername());
        assertEquals(PENGGUNA_PWD, result.getPassword());
    }

    @Test
    public void testLoadUserByNonExistUsernameShouldFail() {
        when(penggunaRepository.findByEmail(NON_EXISTENT_EMAIL)).thenReturn(null);

        assertThrows(UsernameNotFoundException.class,
            () -> {
                userDetailService.loadUserByUsername(NON_EXISTENT_EMAIL);
            });
    }

}
