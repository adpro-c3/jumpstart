package id.c3.adpro.jumpstart.auth.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import id.c3.adpro.jumpstart.auth.config.JwtUtils;
import id.c3.adpro.jumpstart.auth.handler.EmailVerificationHandler;
import id.c3.adpro.jumpstart.auth.handler.PasswordValidationHandler;
import id.c3.adpro.jumpstart.auth.model.ConfirmationToken;
import id.c3.adpro.jumpstart.auth.model.JwtRequest;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@ExtendWith(MockitoExtension.class)
public class AuthServiceImplTest {

    private final String NON_EXISTENT_EMAIL = "fake@mail.com";
    private final String PENGGUNA_EMAIL = "real@mail.com";
    private final String PENGGUNA_PWD = "realpwd";
    private final String PENGGUNA_NAME = "realname";
    private final String DUMMY_TOKEN = "dummytoken";
    @InjectMocks
    AuthServiceImpl authService;
    @Mock
    PenggunaRepository penggunaRepository;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private EmailVerificationHandler emailVerificationHandler;
    @Mock
    private PasswordValidationHandler passwordValidationHandler;
    @Mock
    private UserDetailService userDetailService;
    @Mock
    private JwtUtils jwtUtils;
    private JwtRequest jwtRequest;
    private Pengguna pengguna;
    private UserDetails penggunaUser;
    private ConfirmationToken confirmationToken;

    @BeforeEach
    public void setUp() {
        pengguna = new Pengguna(
            PENGGUNA_EMAIL,
            PENGGUNA_PWD
        );
        confirmationToken = new ConfirmationToken(pengguna);
        jwtRequest = new JwtRequest(PENGGUNA_EMAIL, PENGGUNA_PWD);
        penggunaUser = new User(PENGGUNA_EMAIL, PENGGUNA_PWD, new LinkedList<>());
    }

    @Test
    public void testGetNameExistUsernameShouldSucceed() {
        pengguna.setNama(PENGGUNA_NAME);
        when(penggunaRepository.findByEmail(PENGGUNA_EMAIL)).thenReturn(pengguna);

        String namaPengguna = authService.getName(PENGGUNA_EMAIL);

        assertEquals(pengguna.getNama(), namaPengguna);
    }

    @Test
    public void testGetNamaNonExistUsernameShouldFail() {
        when(penggunaRepository.findByEmail(NON_EXISTENT_EMAIL)).thenReturn(null);

        assertThrows(NullPointerException.class,
            () -> {
                authService.getName(NON_EXISTENT_EMAIL);
            });
    }

    @Test
    public void testIfNonAdminUserIsAdminShouldReturnFalse() {
        pengguna.setAdmin(false);
        when(penggunaRepository.findByEmail(PENGGUNA_EMAIL)).thenReturn(pengguna);

        Boolean isAdmin = authService.isAdmin(PENGGUNA_EMAIL);

        assertFalse(isAdmin);

    }

    @Test
    public void testIfAdminUserIsAdminShouldReturnTrue() {
        pengguna.setAdmin(true);
        when(penggunaRepository.findByEmail(PENGGUNA_EMAIL)).thenReturn(pengguna);
        Boolean isAdmin = authService.isAdmin(PENGGUNA_EMAIL);
        assertTrue(isAdmin);
    }

    @Test
    public void testGetUserDataShouldReturnTheCorrectJSON() {
        pengguna.setNama(PENGGUNA_NAME);
        pengguna.setAdmin(false);

        when(penggunaRepository.findByEmail(PENGGUNA_EMAIL)).thenReturn(pengguna);

        Map<String, String> userData = authService.getUserData(PENGGUNA_EMAIL, DUMMY_TOKEN);

        assertEquals(3, userData.size());
        assertEquals(DUMMY_TOKEN, userData.get("token"));
        assertEquals(PENGGUNA_NAME, userData.get("nama"));
        assertFalse(Boolean.valueOf(userData.get("isAdmin")));
    }

    @Test
    public void testConfirmUserByTokenShouldChangeUserConfrmationStatus() {
        assertFalse(pengguna.isConfirmed());
        when(penggunaRepository.findByEmail(anyString())).thenReturn(pengguna);
        authService.setConfirmed(confirmationToken);
        assertTrue(pengguna.isConfirmed());
    }

    @Test
    public void testServiceAuthenticate() throws Exception{
        pengguna.setConfirmed(true);
        pengguna.setNama(PENGGUNA_NAME);
        pengguna.setAdmin(false);
        Map<String, String> returnValue = new HashMap<>();
        returnValue.put("nama", PENGGUNA_NAME);
        returnValue.put("isAdmin", "false");
        returnValue.put("token", DUMMY_TOKEN);
        when(penggunaRepository.findByEmail(PENGGUNA_EMAIL)).thenReturn(pengguna);
        Map<String, String> userData = authService.getUserData(PENGGUNA_EMAIL, DUMMY_TOKEN);
        when(emailVerificationHandler.getToken()).thenReturn(DUMMY_TOKEN);
        Map<String, String> result = authService.authenticate(jwtRequest);
        assertEquals(returnValue, result);
    }
}
