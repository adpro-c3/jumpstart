package id.c3.adpro.jumpstart.auth.config;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.c3.adpro.jumpstart.auth.service.UserDetailService;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;


@ExtendWith(MockitoExtension.class)
public class JwtRequestFilterTest {

    private final String PENGGUNA_EMAIL = "real@mail.com";
    private final String PENGGUNA_PWD = "realpwd";
    private final String DUMMY_TOKEN = "dummytoken";
    @InjectMocks
    JwtRequestFilter jwtRequestFilter;
    @Mock
    UserDetailService userDetailService;
    @Mock
    JwtUtils jwtUtils;
    private UserDetails userDetails;
    private Pengguna pengguna;
    private MockHttpServletRequest request;

    @BeforeEach
    public void setUp() {
        pengguna = new Pengguna(PENGGUNA_EMAIL, PENGGUNA_PWD);

        request = new MockHttpServletRequest();
        userDetails = new User(PENGGUNA_EMAIL, PENGGUNA_PWD, new ArrayList<>());
        request.addHeader("Authorization", "Bearer " + DUMMY_TOKEN);

        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    public void testJwtRequestFilterWithCorrectTokenShouldSucceed()
        throws IOException, ServletException {
        when(jwtUtils.getUsernameFromToken(DUMMY_TOKEN)).thenReturn(PENGGUNA_EMAIL);
        when(userDetailService.loadUserByUsername(PENGGUNA_EMAIL)).thenReturn(userDetails);
        when(jwtUtils.validateToken(DUMMY_TOKEN, userDetails)).thenReturn(true);

        jwtRequestFilter
            .doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());
    }

    @Test
    public void testJwtRequestFilterWithNoBearerShouldFail() throws
        IOException, ServletException, IllegalArgumentException {
        request = new MockHttpServletRequest();

        jwtRequestFilter
            .doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());

        verify(userDetailService, times(0)).loadUserByUsername(PENGGUNA_EMAIL);
    }

    @Test
    public void testJwtRequestFilterHeaderDoesntStartWithBearerShouldFail()
        throws IOException, ServletException {
        request = new MockHttpServletRequest();
        request.addHeader("Authorization", "SOMETHING - SOMETHING NOT BEARER");

        jwtRequestFilter
            .doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());
        verify(userDetailService, times(0)).loadUserByUsername(PENGGUNA_EMAIL);
    }

    @Test
    public void testJwtRequestFilterWithExistingAuthenticationShouldFail()
        throws IOException, ServletException {
        when(jwtUtils.getUsernameFromToken(DUMMY_TOKEN)).thenReturn(PENGGUNA_EMAIL);
        SecurityContextHolder.getContext()
            .setAuthentication(new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities()));

        jwtRequestFilter
            .doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());

        verify(userDetailService, times(0)).loadUserByUsername(PENGGUNA_EMAIL);
    }

    @Test
    public void testJwtRequestFilterWithTokenInvalid() throws ServletException, IOException {
        when(jwtUtils.getUsernameFromToken(DUMMY_TOKEN)).thenReturn(PENGGUNA_EMAIL);
        when(userDetailService.loadUserByUsername(PENGGUNA_EMAIL)).thenReturn(userDetails);
        when(jwtUtils.validateToken(DUMMY_TOKEN, userDetails)).thenReturn(false);

        jwtRequestFilter
            .doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());
    }
}
