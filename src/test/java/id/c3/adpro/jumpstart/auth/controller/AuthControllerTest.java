package id.c3.adpro.jumpstart.auth.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.c3.adpro.jumpstart.auth.config.JwtUtils;
import id.c3.adpro.jumpstart.auth.model.ConfirmationToken;
import id.c3.adpro.jumpstart.auth.model.JwtRequest;
import id.c3.adpro.jumpstart.auth.model.MailType;
import id.c3.adpro.jumpstart.auth.repository.ConfirmationTokenRepository;
import id.c3.adpro.jumpstart.auth.service.AuthServiceImpl;
import id.c3.adpro.jumpstart.auth.service.NotifyService;
import id.c3.adpro.jumpstart.auth.service.UserDetailService;
import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import id.c3.adpro.jumpstart.core.repository.PenggunaRepository;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = AuthController.class)
@ExtendWith(MockitoExtension.class)
public class AuthControllerTest {

    final private String USERNAME = "regis.jumpstart@gmail.com";
    final private String PASSWORD = "PWD";
    final private String NAMA = "NAMA";
    final private String DUMMY_TOKEN = "DUMMYTOKEN";
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private NotifyService notifyService;
    @MockBean
    private AuthServiceImpl authService;
    @MockBean
    private UserDetailService userDetailService;
    @MockBean
    private JwtUtils jwtUtils;
    @MockBean
    private AuthenticationManager authenticationManager;
    @MockBean
    private PenggunaRepository penggunaRepository;
    @MockBean
    private ConfirmationTokenRepository confirmationTokenRepository;
    private JwtRequest jwtRequest;
    private Pengguna pengguna;
    private UserDetails penggunaUser;

    @BeforeEach
    public void setUp() {
        jwtRequest = new JwtRequest(USERNAME, PASSWORD);
        pengguna = new Pengguna(USERNAME, PASSWORD);
        penggunaUser = new User(USERNAME, PASSWORD, new LinkedList<>());
    }

    private String registerDataJson() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        Map<String, String> mapData = new HashMap<>();
        mapData.put("username", USERNAME);
        mapData.put("password", PASSWORD);
        mapData.put("nama", NAMA);
        return objectMapper.writeValueAsString(mapData);
    }

    @Test
    public void testLoginShouldSucceed() throws Exception {
        Map<String, String> returnValue = new HashMap<>();
        when(authenticationManager.authenticate(any())).thenReturn(null);
        when(userDetailService.loadUserByUsername(anyString())).thenReturn(null);
        pengguna.setConfirmed(true);
        when(penggunaRepository.findByEmail(anyString())).thenReturn(pengguna);
        when(jwtUtils.generateToken(any())).thenReturn("Token");
        when(authService.getUserData(anyString(), anyString())).thenReturn(returnValue);

        mvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(jwtRequest)))
            .andExpect(status().isOk());
    }

    @Test
    public void testLoginWrongEmailOrPasswordShouldFail() throws Exception{
        when(authService.authenticate(any())).thenThrow(new Exception("Password didn't match"));

        mvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(jwtRequest)))
            .andExpect(status().isForbidden())
            .andExpect(content().string("Password didn't match"));
    }

    @Test
    public void testLoginUnverifiedShouldFail() throws Exception{
        when(authService.authenticate(any())).thenThrow(new Exception("Please verify your email address"));

        mvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(jwtRequest)))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("Please verify your email address"));
    }

    @Test
    public void testRegisterNewAccountShouldSucceed() throws Exception {
        when(penggunaRepository.findByEmail(USERNAME)).thenReturn(null);

        mvc.perform(post("/register")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(registerDataJson()))
            .andExpect(status().isOk())
            .andExpect(
                content().string("Registrasi Berhasil, silakan cek email Anda untuk konfirmasi"));
    }

    @Test
    public void testRegisterNewAccountButError() throws Exception {
        when(penggunaRepository.findByEmail(USERNAME)).thenReturn(null);
        doThrow(new IllegalStateException()).when(notifyService).notify(any(MailType.class), any());

        mvc.perform(post("/register")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(registerDataJson()))
            .andExpect(status().isBadRequest())
            .andExpect(
                content().string("Failed to record registration data"));
    }

    @Test
    public void testRegisterExistingAccountShouldFail() throws Exception {
        when(penggunaRepository.findByEmail(USERNAME)).thenReturn(pengguna);

        mvc.perform(post("/register")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(registerDataJson()))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("Your email address is already registered"));
    }

    @Test
    public void testConfirmAccountRegistrationByCorrectTokenShouldSucceed() throws Exception {
        ConfirmationToken confirmationToken = new ConfirmationToken(pengguna);
        when(confirmationTokenRepository.findByConfirmationToken(any(String.class)))
            .thenReturn(confirmationToken);

        mvc.perform(get("/confirm-account?token=" + "token")
            .contentType(MediaType.ALL))
            .andExpect(status().isOk())
            .andExpect(content().string("Akun berhasil dikonfirmasi"));
    }

    @Test
    public void testConfirmAccountRegistrationByWrongTokenShouldFail() throws Exception {
        mvc.perform(get("/confirm-account?token=" + "token")
            .contentType(MediaType.ALL))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("Token yang diberikan tidak valid"));
    }

}
