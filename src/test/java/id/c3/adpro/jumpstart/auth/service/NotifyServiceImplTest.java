package id.c3.adpro.jumpstart.auth.service;

import id.c3.adpro.jumpstart.auth.model.MailType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class NotifyServiceImplTest {

    @InjectMocks
    NotifyServiceImpl notifyService;

    @Test
    public void testPingMailer() {
        notifyService.ping();
    }

    @Test
    public void testSendConfirmationMail() {
        String[] args = new String[]{"Dummy", "person@example.com", "token"};
        notifyService.notify(MailType.CONFIRMATION, args);
    }

    @Test
    public void testSendNotificationMail() {
        String[] args = new String[]{"Dummy", "person@example.com", "dummy_job", "dummy_skill"};
        notifyService.notify(MailType.NOTIFY, args);
    }

    @Test
    public void testSendRecruitmentMail() {
        String[] args = new String[]{"Dummy", "person@example.com", "dummy_job", "accepted/rejected"};
        notifyService.notify(MailType.RECRUITMENT, args);
    }

    @Test
    public void testSendApplicationMail() {
        String[] args = new String[]{"Dummy", "person@example.com", "dummy_job", "accepted/rejected"};
        notifyService.notify(MailType.APPLICATION, args);
    }

}