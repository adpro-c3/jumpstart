package id.c3.adpro.jumpstart.auth.model;

import static org.junit.Assert.assertEquals;

import id.c3.adpro.jumpstart.core.model.entities.Pengguna;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ConfirmationTokenTest {

    final private Pengguna pengguna1 = new Pengguna("EMAIL1", "PWD1");
    final private Pengguna pengguna2 = new Pengguna("EMAIL2", "PWD2");
    private ConfirmationToken confirmationToken1;
    private ConfirmationToken confirmationToken2;
    private ConfirmationToken emptyToke = new ConfirmationToken();

    @BeforeEach
    public void setUp() {
        this.confirmationToken1 = new ConfirmationToken(pengguna1);
        this.confirmationToken2 = new ConfirmationToken(pengguna2);
    }

    @Test
    public void getConfirmationTokenShouldReturnFixedLengthToken() throws Exception {
        assertEquals(36, confirmationToken1.getConfirmationToken().length());
        assertEquals(36, confirmationToken2.getConfirmationToken().length());
    }

    @Test
    public void getCreatedDateShouldReturnDateType() throws Exception {
        assertEquals(Date.class, confirmationToken2.getCreatedDate().getClass());
    }

    @Test
    public void getPenggunaShouldReturnCorrectPengguna() throws Exception {
        assertEquals("EMAIL1", confirmationToken1.getPengguna().getEmail());
        assertEquals("EMAIL2", confirmationToken2.getPengguna().getEmail());
        assertEquals("PWD1", confirmationToken1.getPengguna().getPassword());
        assertEquals("PWD2", confirmationToken2.getPengguna().getPassword());
    }

    @Test
    public void setConfirmationTokenShouldChangeTheToken() throws Exception {
        final String NEWTOKEN = "NEWDUMMYTOKEN";
        confirmationToken1.setConfirmationToken(NEWTOKEN);

        assertEquals(NEWTOKEN, confirmationToken1.getConfirmationToken());
    }

    @Test
    public void setCreatedDateShouldChangeTheCreatedDate() throws Exception {
        final Date NEWDATE = new Date();
        confirmationToken1.setCreatedDate(NEWDATE);

        assertEquals(NEWDATE, confirmationToken1.getCreatedDate());
    }

    @Test
    public void setPenggunaShouldChangeThePengguna() throws Exception {
        confirmationToken1.setPengguna(pengguna2);

        assertEquals("EMAIL2", confirmationToken1.getPengguna().getEmail());
        assertEquals("PWD2", confirmationToken1.getPengguna().getPassword());
    }

}
