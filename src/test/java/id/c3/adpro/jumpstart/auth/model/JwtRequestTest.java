package id.c3.adpro.jumpstart.auth.model;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JwtRequestTest {

    private final String PENGGUNA_EMAIL = "real@email.com";
    private final String PENGGUNA_PWD = "realpwd";
    private JwtRequest jwtRequest;
    private JwtRequest jwtRequest2;
    private JwtRequest emptyJwtRequest = new JwtRequest();

    @BeforeEach
    public void setUp() {
        jwtRequest = new JwtRequest(PENGGUNA_EMAIL, PENGGUNA_PWD);
        jwtRequest2 = new JwtRequest(PENGGUNA_EMAIL, PENGGUNA_PWD, "BUDI");
    }

    @Test
    public void testGetUsernameShouldReturnCorrectUsername() {
        assertEquals(PENGGUNA_EMAIL, jwtRequest.getUsername());
    }

    @Test
    public void testSetUsernameShouldChangeTheUsername() {
        final String NEW_PENGGUNA_EMAIL = "newMail@email.com";

        jwtRequest.setUsername(NEW_PENGGUNA_EMAIL);

        assertEquals(NEW_PENGGUNA_EMAIL, jwtRequest.getUsername());
    }

    @Test
    public void testGetPasswordShouldReturnCorrectPassword() {
        assertEquals(PENGGUNA_PWD, jwtRequest.getPassword());
    }

    @Test
    public void testSetPasswordShouldChangeThePassword() {
        final String NEW_PENGGUNA_PWD = "newpwd";

        jwtRequest.setPassword(NEW_PENGGUNA_PWD);

        assertEquals(NEW_PENGGUNA_PWD, jwtRequest.getPassword());
    }

    @Test
    public void testGetNamaShouldReturnCorrectPassword() {
        assertEquals("BUDI", jwtRequest2.getNama());
    }

}
